-- Create Database
-- CREATE DATABASE NSCRoomReservation;
-- GO

-- Create Rooms table
CREATE TABLE Rooms(
    ID INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
    RoomName VARCHAR(50) NOT NULL,
    SeatingCapacity SMALLINT NOT NULL,
    HasProjector BIT NOT NULL  CONSTRAINT NSCHasProjector DEFAULT 0,
    HasInternet BIT CONSTRAINT NSCHasNet DEFAULT 0,
    HasPodium BIT CONSTRAINT NSCHasPodium  DEFAULT 0,
    RoomSize VARCHAR(2) NOT NULL,
    CreatedAt DATETIME NOT NULL,
    UpdatedAt TIMESTAMP NOT NULL,
)
GO

-- Create Users Table
CREATE TABLE Users(
    ID INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
    FirstName VARCHAR(50) NOT NULL,
    LastName VARCHAR(50) NOT NULL,
    NationalIDNumber INT NOT NULL,
    StaffNumber INT NOT NULL,
    PhoneNumber VARCHAR(15) NOT NULL,
    Email VARCHAR(50) NOT NULL,
    EncryptedPassword VARBINARY(50) NOT NULL,
    Username VARCHAR(50) NOT NULL,
    UserRole SMALLINT CONSTRAINT NSCUsrRole DEFAULT 0 NOT NULL, 
    CreatedAt DATETIME NOT NULL,
    UpdatedAt TIMESTAMP NOT NULL,
    LastLoginAt DATETIME
)
GO

-- Create Table Booking 
CREATE TABLE Bookings(
    ID INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
    RoomID INT NOT NULL,
    UserID INT NOT NULL,
    Pax SMALLINT NOT NULL,
    CompanyName VARCHAR(50) NOT NULL,
    StartDate DATE NOT NULL,
    DaysBooked FLOAT NOT NULL,
    Purpose TEXT NOT NULL,
    Amount FLOAT NOT NULL,
    Phone VARCHAR(20) NOT NULL,
    Package VARCHAR(20) NOT NULL,
    PaymentType SMALLINT NOT NULL,
    CreatedAt DATETIME NOT NULL,
    UpdatedAt TIMESTAMP NOT NULL
)
GO