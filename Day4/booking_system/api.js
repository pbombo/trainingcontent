const express = require('express')
const sql = require('mssql')
const cors = require('cors')
const bodyParser = require('body-parser')
// Create app
const app = express()
app.use(cors())

// parse application/json
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

// Create connection to database
const config = {
    user: 'sa', // update me
    password: 'root', // update me
    server: 'localhost',
    database: 'NSCRoomReservation',
    parseJSON: true,
    options: {
        encrypt: false,
        // rowCollectionOnRequestCompletion: true
    }
}

// Reusable connect function

const sqlRequest = async function(query) {
    // connect to your database
    try {
        const pool = await sql.connect(config)
        const result = await pool.request().query(`${query} FOR JSON AUTO`)
        await sql.close()
        return result.recordset[0]
    } catch(err) {
        console.log(err)
        return err
    } 
}

// Routes here
app.get('/rooms', function (request, res) {
    // Read all rows from table
    const query = 'SELECT ID, RoomName, SeatingCapacity, HasProjector,HasInternet, HasPodium, RoomSize, CreatedAt, UpdatedAt FROM Rooms'
    sqlRequest(query)
        .then(function(result) {
            res.json(result)
        })

})

app.get('/rooms/:roomId', function (req, res) {
    // Get specific row
    const query = 'SELECT ID, RoomName, SeatingCapacity, HasProjector,HasInternet, HasPodium, RoomSize, CreatedAt, UpdatedAt FROM Rooms WHERE ID = ' + req.params.roomId
    sqlRequest(query)
        .then(function (result) {
            res.json(result)
        })
    
})

app.post('/rooms', function (req, res) {
    const {
        RoomName, SeatingCapacity, HasProjector, HasInternet, HasPodium, RoomSize, CreatedAt, UpdatedAt
    } = req.body
    // Get specific row
    const query = `INSERT INTO ID, RoomName, SeatingCapacity, HasProjector, HasInternet, HasPodium, RoomSize, CreatedAt, UpdatedAt 
    VALUES (${RoomName}, ${SeatingCapacity}, ${HasProjector}, ${HasInternet}, ${HasPodium}, ${RoomSize})`
    sqlRequest(query)
        .then(function (result) {
            res.json(result)
        })
})


// app.post('/rooms', function(req, res) {
//     // Read all rows from table
//     const query = 'SELECT ID, RoomName, SeatingCapacity, HasProjector,HasInternet, HasPodium, RoomSize, CreatedAt, UpdatedAt FROM Rooms'
//     sqlRequest(query)
//         .then(function(result) {
//             // console.log(result[0].UpdatedAt.toString('utf8'))
//             res.json(result)
//         })    
// })


app.listen('3000', () => console.log('Express started'))