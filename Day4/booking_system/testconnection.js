const Connection = require('tedious').Connection;
const Request = require('tedious').Request;
const TYPES = require('tedious').TYPES;

// Create connection to database
const config = {
    userName: 'sa', // update me
    password: 'root', // update me
    server: 'localhost',
    options: {
        encrypt: false,
        database: 'NSCRoomReservation'
    }
}
const connection = new Connection(config);

function Read(callback) {
// Attempt to connect and execute queries if connection goes through
// Read all rows from table
const query = `SELECT[ID]
        , [RoomName]
        , [SeatingCapacity]
        , [HasProjector]
        , [HasInternet]
        , [HasPodium]
        , [RoomSize]
        , [CreatedAt]
        , [UpdatedAt]
    FROM[dbo].[Rooms]`
request = new Request(
    query,
    function (err, rowCount, rows) {
        if (err) {
            console.log(err);
        } else {
            console.log(rowCount + ' row(s) returned');
        }
    });

// Print the rows read
let result = "";
request.on('row', function (columns) {
    columns.forEach(function (column) {
        if (column.value === null) {
            console.log('NULL');
        } else {
            result += column.value + " ";
        }
    });
    console.log(result);
    result = "";
});

// Execute SQL statement
connection.execSql(request);
}

// Attempt to connect and execute queries if connection goes through
connection.on('connect', function (err) {
    if (err) {
        console.log(err);
    } else {
        console.log('Connected');

        // Execute all functions in the array serially
        Read()
    }
});