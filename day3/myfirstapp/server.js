const express = require('express')
const template = require('express-handlebars')

const app = express()

app.engine('handlebars', template({ defaultLayout: 'main' }))
app.set('view engine', 'handlebars')

// My application's routes
app.get('/', function(request, response) {
	response.render('test');
})

app.get('/about', (req, res) => res.send('About!'))

app.get('/help', (req, res) => res.send('Help!'))

// Start the application
app.listen('1234', function() { console.log('App has started')})
