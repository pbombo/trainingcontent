<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class UserController extends Controller
{

    public function index() 
    {
        return view('welcome');
    }

    public function show($name, $location)
    {
        $town = 'Malindi'; // Assume this has been fetched to a DB
        if (strtolower($location) == 'mombasa') {
            $location  = 'Welcome to '. $town;
        } else {
            $location = $name . ', you should come to '. $town;
        }
        return view('users/test', ['name' => $name, 'loc' => $location]);
    }
}

