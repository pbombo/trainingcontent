<?php

namespace App\Http\Middleware;

use Closure;

class CheckName
{

    public function handle($request, Closure $next)
    {
        if ($request->name != 'Paul') {
            return redirect('/access-denied');
        }

        return $next($request);
    }
}