<html>
    <head>
        <title>My first laravel</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
                padding: 2em;
            }
            </style>
    </head>

    <body>
        <header>
            <nav>
                Hello <b><?php echo $name; ?></b>!  
            </nav>
        </header>
        <hr />
        <h1><?php echo $loc; ?></h1>
    </body>

</html>