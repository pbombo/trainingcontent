<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/access-denied', function () {
    return view('unauthorized');
});

Route::get('/hello/{name}', function ($name) {
    return view('users/test', ['name' => $name, 'loc' => 'Marsabit']);
})->middleware('check_name');

Route::get('/users/{name}', function ($name) {
    return view('users/test', ['name' => $name]);
});

Route::get('helb', 'UserController@index');
Route::get('u/{name}/{location}', 'UserController@show');

Route::resource('photos', 'PhotoController');