<?php

require 'vendor/autoload.php';

use AfricasTalking\SDK\AfricasTalking;

$username = "sandbox";

$apiKey = "6bbb419495456a2951031a0546f399cbb3d2ca629db5714f763ecf8b66544dbb";

$AT = new AfricasTalking($username, $apiKey);

$SMS = $AT->sms();

$people = array(
    ['number' => '+254724486439', 'name' => 'Paul'], 
    [ 'number' => '+254727045828', 'name' => 'Tony']
);

$message = 'Hello ';

foreach ($people as $person) {
    var_dump($message . $person['name']);
    $SMS->send(['message'=> $message . $person['name'], 'to' => $person['number']]);
}